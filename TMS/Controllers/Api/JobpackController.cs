﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TMS.Models;

namespace TMS.Controllers.Api
{
    [Route("api/jobpack")]
    [ApiController]
    public class JobpackController : ControllerBase
    {
        // GET: api/Jobpack
        [HttpGet("all")]
        public IEnumerable<Jobpack> Get()
        {
            IEnumerable<Jobpack> data = new List<Jobpack>();

            using (ttmmssContext context = new ttmmssContext())
            {
                data = context.Jobpack.ToList();
            }

            return data;
        }

        // GET: api/Jobpack/5
        [HttpGet("{id}")]
        public Jobpack Get(int id)
        {
            Jobpack jobpack = null;

            using (ttmmssContext context = new ttmmssContext())
            {
                jobpack = context.Jobpack.Where(x => x.Id == id).FirstOrDefault();
            }

            return jobpack;
        }

        // POST: api/Jobpack
        [HttpPost]
        public Jobpack Create([FromBody] Jobpack jobpack)
        {
            using (ttmmssContext context = new ttmmssContext())
            {
                context.Add(jobpack);
                context.SaveChanges();
            }

            return jobpack;
        }

        // PUT: api/Jobpack/5
        [HttpPut]
        public bool Put([FromBody] Jobpack jobpack)
        {
            using (ttmmssContext context = new ttmmssContext())
            {
                Jobpack updatingJobpack = context.Jobpack.Where(x => x.Id == jobpack.Id).FirstOrDefault();
                updatingJobpack = jobpack;
                context.SaveChanges();
            }

            return true;
        }

        // DELETE
        [HttpDelete]
        public bool Delete([FromBody] Jobpack jobpack)
        {
            using (ttmmssContext context = new ttmmssContext())
            {
                context.Jobpack.Remove(jobpack);
                context.SaveChanges();
            }

            return true;
        }
    }
}
