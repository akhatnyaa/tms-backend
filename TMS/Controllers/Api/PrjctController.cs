﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using TMS.Models;

namespace TMS.Controllers.Api
{
    [Route("api/prjct")]
    [ApiController]
    public class PrjctController : ControllerBase
    {
        // GET: api/Afc
        [HttpGet("all")]
        public IEnumerable<Prjct> Get()
        {
            IEnumerable<Prjct> data = new List<Prjct>();

            using (ttmmssContext context = new ttmmssContext())
            {
                data = context.Prjct.ToList();
            }

            return data;
        }

        // GET: api/Prjct/5
        [HttpGet("{text}")]
        public Prjct Search(string text)
        {
            Prjct tod = null;

            using (ttmmssContext context = new ttmmssContext())
            {
                tod = context.Prjct.Where(x => x.FullName.Contains(text)).FirstOrDefault();
            }

             return tod;
        }

        // POST: api/Afc
        [HttpPost]
        public Prjct Create([FromBody] Prjct prjct)
        {
            using (ttmmssContext context = new ttmmssContext())
            {
                context.Add(prjct);
                context.SaveChanges();
            }

            return prjct;
        }

        // PUT: api/Afc/5
        [HttpPut]
        public bool Put([FromBody] Prjct prjct)
        {
            using (ttmmssContext context = new ttmmssContext())
            {
                Prjct updatingTod = context.Prjct.Where(x => x.Id == prjct.Id).FirstOrDefault();
                updatingTod = prjct;
                context.SaveChanges();
            }

            return true;
        }

        // DELETE
        [HttpDelete]
        public bool Delete([FromBody] Prjct prjct)
        {
            using (ttmmssContext context = new ttmmssContext())
            {
                context.Prjct.Remove(prjct);
                context.SaveChanges();
            }

            return true;
        }
    }
}
