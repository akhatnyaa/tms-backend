﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TMS.Models;

namespace TMS.Controllers.Api
{
    [Route("api/inbox")]
    [ApiController]
    public class InboxController : ControllerBase
    {
        // GET: api/Inbox
        [HttpGet("all")]
        public IEnumerable<Inbox> Get()
        {
            IEnumerable<Inbox> data = new List<Inbox>();

            using (ttmmssContext context = new ttmmssContext())
            {
                data = context.Inbox.ToList();
            }

            return data;
        }

        // GET: api/Inbox/5
        [HttpGet("{id}")]
        public Inbox Get(int id)
        {
            Inbox inbox = null;

            using (ttmmssContext context = new ttmmssContext())
            {
                inbox = context.Inbox.Where(x => x.Id == id).FirstOrDefault();
            }

            return inbox;
        }

        // POST: api/Inbox
        [HttpPost]
        public Inbox Create([FromBody] Inbox inbox)
        {
            using (ttmmssContext context = new ttmmssContext())
            {
                context.Add(inbox);
                context.SaveChanges();
            }

            return inbox;
        }

        // PUT: api/Inbox/5
        [HttpPut]
        public bool Put([FromBody] Inbox inbox)
        {
            using (ttmmssContext context = new ttmmssContext())
            {
                Inbox updatingInbox = context.Inbox.Where(x => x.Id == inbox.Id).FirstOrDefault();
                updatingInbox = inbox;
                context.SaveChanges();
            }

            return true;
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete]
        public bool Delete([FromBody] Inbox inbox)
        {
            using (ttmmssContext context = new ttmmssContext())
            {
                context.Inbox.Remove(inbox);
                context.SaveChanges();
            }

            return true;
        }
    }
}
