﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using TMS.Models;

namespace TMS.Controllers.Api
{
    [Route("api/afc")]
    [ApiController]
    public class AfcController : ControllerBase
    {
        // GET: api/Afc
        [HttpGet("all")]
        public IEnumerable<Afc> Get()
        {
            IEnumerable<Afc> data = new List<Afc>();

            using (ttmmssContext context = new ttmmssContext())
            {
                data = context.Afc.ToList();
            }

            return data;
        }

        // GET: api/Afc/5
        [HttpGet("{id}")]
        public Afc Get(int id)
        {
            Afc tod = null;

            using (ttmmssContext context = new ttmmssContext())
            {
                tod = context.Afc.Where(x => x.Id == id).FirstOrDefault();
            }

            return tod;
        }

        // POST: api/Afc
        [HttpPost]
        public Afc Create([FromBody] Afc afc)
        {
            using (ttmmssContext context = new ttmmssContext())
            {
                context.Add(afc);
                context.SaveChanges();
            }

            return afc;
        }

        // PUT: api/Afc/5
        [HttpPut]
        public bool Put([FromBody] Afc afc)
        {
            using (ttmmssContext context = new ttmmssContext())
            {
                Afc updatingTod = context.Afc.Where(x => x.Id == afc.Id).FirstOrDefault();
                updatingTod = afc;
                context.SaveChanges();
            }

            return true;
        }

        // DELETE
        [HttpDelete]
        public bool Delete([FromBody] Afc afc)
        {
            using (ttmmssContext context = new ttmmssContext())
            {
                context.Afc.Remove(afc);
                context.SaveChanges();
            }

            return true;
        }
    }
}
