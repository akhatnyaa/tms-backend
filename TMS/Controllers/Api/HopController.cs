﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.AspNetCore.Mvc;
using TMS.Models;

namespace TMS.Controllers.Api
{
    [Route("api/hop")]
    [ApiController]
    public class HopController : ControllerBase
    {
        // GET: api/Hop
        [HttpGet("all")]
        public IEnumerable<Hop> Get()
        {
            IEnumerable<Hop> data = new List<Hop>();

            using (ttmmssContext context = new ttmmssContext())
            {
                data = context.Hop.ToList();
            }

            return data;
        }

        // GET: api/Hop/5
        [HttpGet("{id}")]
        public Hop Get(int id)
        {
            Hop tod = null;

            using (ttmmssContext context = new ttmmssContext())
            {
                tod = context.Hop.Where(x => x.Id == id).FirstOrDefault();
            }

            return tod;
        }

        // POST: api/AfcHop
        [HttpPost]
        public Hop Create([FromBody] Hop hop)
        {
            using (ttmmssContext context = new ttmmssContext())
            {
                context.Add(hop);
                context.SaveChanges();
            }

            return hop;
        }

        // PUT: api/AfcHop/5
        [HttpPut]
        public bool Put([FromBody] Hop hop)
        {
            using (ttmmssContext context = new ttmmssContext())
            {
                Hop updatingTod = context.Hop.Where(x => x.Id == hop.Id).FirstOrDefault();
                updatingTod = hop;
                context.SaveChanges();
            }

            return true;
        }

        // DELETE
        [HttpDelete]
        public bool Delete([FromBody] Hop hop)
        {
            using (ttmmssContext context = new ttmmssContext())
            {
                context.Hop.Remove(hop);
                context.SaveChanges();
            }

            return true;
        }
    }
}
