﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using TMS.Models;

namespace TMS.Controllers.Api
{
    [Route("api/tod")]
    [ApiController]
    public class TodController : ControllerBase
    {
        // GET: api/Tod
        [HttpGet("all")]
        public IEnumerable<Tod> Get()
        {
            IEnumerable<Tod> data = new List<Tod>();

            using (ttmmssContext context = new ttmmssContext())
            {
                data = context.Tod.ToList();
            }

            return data;
        }

        // GET: api/tod/5
        [HttpGet("{id}")]
        public Tod Get(int id)
        {
            Tod tod = null;

            using (ttmmssContext context = new ttmmssContext())
            {
                tod = context.Tod.Where(x => x.Id == id).FirstOrDefault();
            }

            return tod;
        }

        // POST: api/Tod
        [HttpPost]
        public Tod Create([FromBody] Tod tod)
        {
            using (ttmmssContext context = new ttmmssContext())
            {
                context.Add(tod);
                context.SaveChanges();
            }

            return tod;
        }

        // PUT: api/Tod/5
        [HttpPut]
        public bool Put([FromBody] Tod tod)
        {
            using (ttmmssContext context = new ttmmssContext())
            {
                Tod updatingTod = context.Tod.Where(x => x.Id == tod.Id).FirstOrDefault();
                updatingTod = tod;
                context.SaveChanges();
            }

            return true;
        }

        // DELETE
        [HttpDelete]
        public bool Delete([FromBody] Tod tod)
        {
            using (ttmmssContext context = new ttmmssContext())
            {
                context.Tod.Remove(tod);
                context.SaveChanges();
            }

            return true;
        }
    }
}
