﻿using System;
using System.Collections.Generic;
using System.Data.Odbc;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Dapper;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver.Core.Configuration;
using TMS.Models;

namespace TMS.Controllers
{
    public class HomeController : Controller
    {
        public object Index()
        {
            return this.Inbox();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        public IActionResult Tod()
        {
            return View();
        }

        public IActionResult Inbox()
        {
            using (ttmmssContext context = new ttmmssContext())
            {
                ViewData["inboxes"] = context.Inbox.AsList();
            }
            return View();
        }

        public IActionResult Jobpack()
        {
            using (ttmmssContext context = new ttmmssContext())
            {
                ViewData["inboxes"] = context.Jobpack.AsList();
            }
            return View();
        }

        public IActionResult AfcHop()
        {
            using (ttmmssContext context = new ttmmssContext())
            {

                return View();
            }
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
