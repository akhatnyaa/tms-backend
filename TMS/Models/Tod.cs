﻿using System;
using System.Collections.Generic;

namespace TMS.Models
{
    public partial class Tod
    {
        public int Id { get; set; }
        public long JpkId { get; set; }
        public int? TodNumber { get; set; }
        public string Name { get; set; }
        public string DescriptionEn { get; set; }
        public string DescriptionRu { get; set; }
        public DateTime? ImcAbrlConfDate { get; set; }
        public string ImcAbrlConfBy { get; set; }
        public string ImcAbrlConfFile { get; set; }
        public DateTime? TechsupConfDate { get; set; }
        public string TechsupConfBy { get; set; }
        public string TechsupConfFile { get; set; }
        public DateTime? CloseDate { get; set; }
        public string CloseBy { get; set; }
        public string Commentss { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
