﻿using System;
using System.Collections.Generic;

namespace TMS.Models
{
    public partial class Jobpack
    {
        public int Id { get; set; }
        public long PrjctId { get; set; }
        public string Name { get; set; }
        public string DescriptionEn { get; set; }
        public string DescriptionRu { get; set; }
        public int DisciplineJpk { get; set; }
        public string Commentss { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
