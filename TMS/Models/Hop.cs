﻿using System;
using System.Collections.Generic;

namespace TMS.Models
{
    public partial class Hop
    {
        public int Id { get; set; }
        public long AfcId { get; set; }
        public int HopNumber { get; set; }
        public string Name { get; set; }
        public string DescriptionEn { get; set; }
        public string DescriptionRu { get; set; }
        public string SubmittedBy { get; set; }
        public DateTime? RecievedDate { get; set; }
        public string RecievedBy { get; set; }
        public DateTime? PreparedDate { get; set; }
        public string PreparedBy { get; set; }
        public DateTime? CheckedDate { get; set; }
        public string CheckedBy { get; set; }
        public DateTime? ApprovedDate { get; set; }
        public string ApprovedBy { get; set; }
        public string ApprovalPageFile { get; set; }
        public int? CdhcStatus { get; set; }
        public string CdhcFile { get; set; }
        public int? PdhcStatus { get; set; }
        public string PdhcFile { get; set; }
        public DateTime? DrwCheckedDate { get; set; }
        public string DrwCheckedBy { get; set; }
        public string Commentss { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
