﻿using System;
using System.Collections.Generic;

namespace TMS.Models
{
    public partial class Prjct
    {
        public int Id { get; set; }
        public string ShortName { get; set; }
        public string FullName { get; set; }
        public string DescriptionEn { get; set; }
        public string DescriptionRu { get; set; }
        public string GpsCoord { get; set; }
        public string Directory { get; set; }
        public string Commentss { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
        public int PrjGroup { get; set; }
    }
}
