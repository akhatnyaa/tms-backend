﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;

namespace TMS.Models
{
    public partial class ttmmssContext : DbContext
    {
        public ttmmssContext()
        {
        }

        public ttmmssContext(DbContextOptions<ttmmssContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Afc> Afc { get; set; }
        public virtual DbSet<Hop> Hop { get; set; }
        public virtual DbSet<Inbox> Inbox { get; set; }
        public virtual DbSet<Jobpack> Jobpack { get; set; }
        public virtual DbSet<Prjct> Prjct { get; set; }
        public virtual DbSet<ServiceOrder> ServiceOrder { get; set; }
        public virtual DbSet<Tod> Tod { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            if (!optionsBuilder.IsConfigured)
            {
#warning To protect potentially sensitive information in your connection string, you should move it out of source code. See http://go.microsoft.com/fwlink/?LinkId=723263 for guidance on storing connection strings.
                optionsBuilder.UseNpgsql("Host=localhost;Port=5432;Database=ttmmss;Username=tms;Password=password");
            }
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.4-servicing-10062");

            modelBuilder.Entity<Afc>(entity =>
            {
                entity.ToTable("afc");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.BorrowedBy)
                    .IsRequired()
                    .HasColumnName("borrowed_by")
                    .HasMaxLength(50);

                entity.Property(e => e.BorrowedDate)
                    .HasColumnName("borrowed_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.Commentss)
                    .HasColumnName("commentss")
                    .HasMaxLength(550);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("created_by")
                    .HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.DescriptionEn)
                    .IsRequired()
                    .HasColumnName("description_en")
                    .HasMaxLength(150);

                entity.Property(e => e.DescriptionRu)
                    .IsRequired()
                    .HasColumnName("description_ru")
                    .HasMaxLength(150);

                entity.Property(e => e.InbId).HasColumnName("inb_id");

                entity.Property(e => e.JpkId).HasColumnName("jpk_id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);

                entity.Property(e => e.Number).HasColumnName("number");

                entity.Property(e => e.PcgCreatedBy)
                    .IsRequired()
                    .HasColumnName("pcg_created_by")
                    .HasMaxLength(50);

                entity.Property(e => e.PcgCreationDate)
                    .HasColumnName("pcg_creation_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.ServiceOrderId).HasColumnName("service_order_id");

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasColumnName("updated_by")
                    .HasMaxLength(50);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("updated_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");
            });

            modelBuilder.Entity<Hop>(entity =>
            {
                entity.ToTable("hop");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.AfcId).HasColumnName("afc_id");

                entity.Property(e => e.ApprovalPageFile)
                    .HasColumnName("approval_page_file")
                    .HasMaxLength(150);

                entity.Property(e => e.ApprovedBy)
                    .IsRequired()
                    .HasColumnName("approved_by")
                    .HasMaxLength(50);

                entity.Property(e => e.ApprovedDate)
                    .HasColumnName("approved_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.CdhcFile)
                    .HasColumnName("cdhc_file")
                    .HasMaxLength(150);

                entity.Property(e => e.CdhcStatus).HasColumnName("cdhc_status");

                entity.Property(e => e.CheckedBy)
                    .IsRequired()
                    .HasColumnName("checked_by")
                    .HasMaxLength(50);

                entity.Property(e => e.CheckedDate)
                    .HasColumnName("checked_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.Commentss)
                    .HasColumnName("commentss")
                    .HasMaxLength(550);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("created_by")
                    .HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.DescriptionEn)
                    .IsRequired()
                    .HasColumnName("description_en")
                    .HasMaxLength(150);

                entity.Property(e => e.DescriptionRu)
                    .IsRequired()
                    .HasColumnName("description_ru")
                    .HasMaxLength(150);

                entity.Property(e => e.DrwCheckedBy)
                    .IsRequired()
                    .HasColumnName("drw_checked_by")
                    .HasMaxLength(50);

                entity.Property(e => e.DrwCheckedDate)
                    .HasColumnName("drw_checked_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.HopNumber).HasColumnName("hop_number");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);

                entity.Property(e => e.PdhcFile)
                    .HasColumnName("pdhc_file")
                    .HasMaxLength(150);

                entity.Property(e => e.PdhcStatus).HasColumnName("pdhc_status");

                entity.Property(e => e.PreparedBy)
                    .IsRequired()
                    .HasColumnName("prepared_by")
                    .HasMaxLength(50);

                entity.Property(e => e.PreparedDate)
                    .HasColumnName("prepared_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.RecievedBy)
                    .IsRequired()
                    .HasColumnName("recieved_by")
                    .HasMaxLength(50);

                entity.Property(e => e.RecievedDate)
                    .HasColumnName("recieved_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.SubmittedBy)
                    .IsRequired()
                    .HasColumnName("submitted_by")
                    .HasMaxLength(50);

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasColumnName("updated_by")
                    .HasMaxLength(50);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("updated_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");
            });

            modelBuilder.Entity<Inbox>(entity =>
            {
                entity.ToTable("inbox");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CheckedBy)
                    .IsRequired()
                    .HasColumnName("checked_by")
                    .HasMaxLength(355);

                entity.Property(e => e.CheckedDate)
                    .HasColumnName("checked_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.Commentss).HasColumnName("commentss");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("created_by")
                    .HasMaxLength(355);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.DescriptionEn)
                    .IsRequired()
                    .HasColumnName("description_en")
                    .HasMaxLength(355);

                entity.Property(e => e.DescriptionRu)
                    .IsRequired()
                    .HasColumnName("description_ru")
                    .HasMaxLength(355);

                entity.Property(e => e.Distributor).HasColumnName("distributor");

                entity.Property(e => e.InbFile)
                    .HasColumnName("inb_file")
                    .HasMaxLength(255);

                entity.Property(e => e.IssueDate)
                    .HasColumnName("issue_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.Originator).HasColumnName("originator");

                entity.Property(e => e.PrjctId).HasColumnName("prjct_id");

                entity.Property(e => e.ReceivedBy)
                    .IsRequired()
                    .HasColumnName("received_by")
                    .HasMaxLength(355);

                entity.Property(e => e.ReceivedDate)
                    .HasColumnName("received_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.TrmNum).HasColumnName("trm_num");

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasColumnName("updated_by")
                    .HasMaxLength(355);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("updated_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");
            });

            modelBuilder.Entity<Jobpack>(entity =>
            {
                entity.ToTable("jobpack");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Commentss)
                    .HasColumnName("commentss")
                    .HasMaxLength(950);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("created_by")
                    .HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.DescriptionEn)
                    .IsRequired()
                    .HasColumnName("description_en")
                    .HasMaxLength(250);

                entity.Property(e => e.DescriptionRu)
                    .IsRequired()
                    .HasColumnName("description_ru")
                    .HasMaxLength(250);

                entity.Property(e => e.DisciplineJpk).HasColumnName("discipline_jpk");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name");

                entity.Property(e => e.PrjctId).HasColumnName("prjct_id");

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasColumnName("updated_by")
                    .HasMaxLength(50);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("updated_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");
            });

            modelBuilder.Entity<Prjct>(entity =>
            {
                entity.ToTable("prjct");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.Commentss)
                    .IsRequired()
                    .HasColumnName("commentss")
                    .HasMaxLength(355);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("created_by")
                    .HasMaxLength(355);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.DescriptionEn)
                    .IsRequired()
                    .HasColumnName("description_en")
                    .HasMaxLength(355);

                entity.Property(e => e.DescriptionRu)
                    .IsRequired()
                    .HasColumnName("description_ru")
                    .HasMaxLength(355);

                entity.Property(e => e.Directory)
                    .IsRequired()
                    .HasColumnName("directory")
                    .HasMaxLength(355);

                entity.Property(e => e.FullName)
                    .IsRequired()
                    .HasColumnName("full_name")
                    .HasMaxLength(50);

                entity.Property(e => e.GpsCoord)
                    .IsRequired()
                    .HasColumnName("gps_coord")
                    .HasMaxLength(355);

                entity.Property(e => e.PrjGroup).HasColumnName("prj_group");

                entity.Property(e => e.ShortName)
                    .IsRequired()
                    .HasColumnName("short_name")
                    .HasMaxLength(50);

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasColumnName("updated_by")
                    .HasMaxLength(355);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("updated_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");
            });

            modelBuilder.Entity<ServiceOrder>(entity =>
            {
                entity.ToTable("service_order");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CloseDate)
                    .HasColumnName("close_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.Commentss)
                    .HasColumnName("commentss")
                    .HasMaxLength(550);

                entity.Property(e => e.Company).HasColumnName("company");

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("created_by")
                    .HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.DescriptionEn)
                    .IsRequired()
                    .HasColumnName("description_en")
                    .HasMaxLength(250);

                entity.Property(e => e.DescriptionRu)
                    .IsRequired()
                    .HasColumnName("description_ru")
                    .HasMaxLength(250);

                entity.Property(e => e.ExpireDate)
                    .HasColumnName("expire_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.IssueBy)
                    .IsRequired()
                    .HasColumnName("issue_by")
                    .HasMaxLength(50);

                entity.Property(e => e.IssueDate)
                    .HasColumnName("issue_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.SorNumber).HasColumnName("sor_number");

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasColumnName("updated_by")
                    .HasMaxLength(50);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("updated_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");
            });

            modelBuilder.Entity<Tod>(entity =>
            {
                entity.ToTable("tod");

                entity.Property(e => e.Id).HasColumnName("id");

                entity.Property(e => e.CloseBy)
                    .HasColumnName("close_by")
                    .HasMaxLength(50);

                entity.Property(e => e.CloseDate)
                    .HasColumnName("close_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.Commentss)
                    .HasColumnName("commentss")
                    .HasMaxLength(550);

                entity.Property(e => e.CreatedBy)
                    .IsRequired()
                    .HasColumnName("created_by")
                    .HasMaxLength(50);

                entity.Property(e => e.CreatedDate)
                    .HasColumnName("created_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.DescriptionEn)
                    .IsRequired()
                    .HasColumnName("description_en")
                    .HasMaxLength(150);

                entity.Property(e => e.DescriptionRu)
                    .IsRequired()
                    .HasColumnName("description_ru")
                    .HasMaxLength(150);

                entity.Property(e => e.ImcAbrlConfBy)
                    .HasColumnName("imc_abrl_conf_by")
                    .HasMaxLength(50);

                entity.Property(e => e.ImcAbrlConfDate)
                    .HasColumnName("imc_abrl_conf_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.ImcAbrlConfFile)
                    .HasColumnName("imc_abrl_conf_file")
                    .HasMaxLength(150);

                entity.Property(e => e.JpkId).HasColumnName("jpk_id");

                entity.Property(e => e.Name)
                    .IsRequired()
                    .HasColumnName("name")
                    .HasMaxLength(50);

                entity.Property(e => e.TechsupConfBy)
                    .HasColumnName("techsup_conf_by")
                    .HasMaxLength(50);

                entity.Property(e => e.TechsupConfDate)
                    .HasColumnName("techsup_conf_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");

                entity.Property(e => e.TechsupConfFile)
                    .HasColumnName("techsup_conf_file")
                    .HasMaxLength(150);

                entity.Property(e => e.TodNumber).HasColumnName("tod_number");

                entity.Property(e => e.UpdatedBy)
                    .IsRequired()
                    .HasColumnName("updated_by")
                    .HasMaxLength(50);

                entity.Property(e => e.UpdatedDate)
                    .HasColumnName("updated_date")
                    .HasColumnType("date")
                    .HasDefaultValueSql("CURRENT_DATE");
            });
        }
    }
}
