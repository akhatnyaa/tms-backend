﻿using System;
using System.Collections.Generic;

namespace TMS.Models
{
    public partial class ServiceOrder
    {
        public int Id { get; set; }
        public int SorNumber { get; set; }
        public string DescriptionEn { get; set; }
        public string DescriptionRu { get; set; }
        public DateTime? IssueDate { get; set; }
        public string IssueBy { get; set; }
        public DateTime? ExpireDate { get; set; }
        public DateTime? CloseDate { get; set; }
        public int? Company { get; set; }
        public string Commentss { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
