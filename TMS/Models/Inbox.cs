﻿using System;
using System.Collections.Generic;

namespace TMS.Models
{
    public partial class Inbox
    {
        public int Id { get; set; }
        public long PrjctId { get; set; }
        public int TrmNum { get; set; }
        public string DescriptionEn { get; set; }
        public string DescriptionRu { get; set; }
        public DateTime? IssueDate { get; set; }
        public int Originator { get; set; }
        public int Distributor { get; set; }
        public string ReceivedBy { get; set; }
        public DateTime? ReceivedDate { get; set; }
        public string CheckedBy { get; set; }
        public DateTime? CheckedDate { get; set; }
        public string InbFile { get; set; }
        public string Commentss { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
