﻿using System;
using System.Collections.Generic;

namespace TMS.Models
{
    public partial class Afc
    {
        public int Id { get; set; }
        public long JpkId { get; set; }
        public long InbId { get; set; }
        public long ServiceOrderId { get; set; }
        public long? Number { get; set; }
        public string Name { get; set; }
        public string DescriptionEn { get; set; }
        public string DescriptionRu { get; set; }
        public DateTime? PcgCreationDate { get; set; }
        public string PcgCreatedBy { get; set; }
        public DateTime? BorrowedDate { get; set; }
        public string BorrowedBy { get; set; }
        public string Commentss { get; set; }
        public string CreatedBy { get; set; }
        public DateTime? CreatedDate { get; set; }
        public string UpdatedBy { get; set; }
        public DateTime? UpdatedDate { get; set; }
    }
}
